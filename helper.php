<?php

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.module.helper');


final class MLC {

    const COOKIE_NAME =     '__mlc'; // set by www.myleadconverter.com/mlc.js

    public static function send( $fieldset, $params = array() ) {
        $moduleParams = new JParameter( JModuleHelper::getModule('myleadconverter')->params );

        global $ff_processor;
        foreach($ff_processor->submitdata as $d) {
            $name = $d[_FF_DATA_NAME];
            $value = $d[_FF_DATA_VALUE];
            $type = $d[_FF_DATA_TYPE];

            switch($type) {
                case 'Checkbox' :
                case 'Radio' :
                    $value = ($value=='checked') ? 'true' : 'false';
                default:
            }

            if (array_key_exists($name, $params) && !is_array($params[$name])) {
                $params[$name] = array($params[$name], $value);
            } else if (array_key_exists($name, $params)) {
                $params[$name][] = $value;
            } else {
                $params[$name] = $value;
            }
        }

        MyLeadConverter::connect(
            $moduleParams->get('mlcAccountId', ''),
            $moduleParams->get('mlcApiToken', ''),
            $moduleParams->get('debugMode', '0') == '1'
        )->send($fieldset, $params);
    }

    public static function query_stringify( $postVars ) {
    	$q = http_build_query($postVars, '', '&');
    	return preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '[]=', $q);
    }

    public static function stats() {
    	$userData = array();
        parse_str(isset($_COOKIE[self::COOKIE_NAME]) ? $_COOKIE[self::COOKIE_NAME] : '', $userData);
    	return $userData;
    }

}


final class MyLeadConverter {

    const BASE_URL =        'https://www.myleadconverter.com/';
    const SSL =             true;
    const UA =              'php-mlc';
    const API_VERSION =     '1.0';
    const API_REVISION =    '011712';

    var $accountId;
    var $apiToken;
    var $debugMode = false;

    function MyLeadConverter( $id, $token, $debugMode = false ) {
        $this->accountId = $id;
        $this->apiToken = $token;
        $this->debugMode = $debugMode;
    }

    public static function connect( $id, $token, $debugMode = false ) {
       return new MyLeadConverter($id, $token, $debugMode);
    }

    function send( $fieldsetId, $params ) {
        $params['mlc__fieldset'] = $fieldsetId;
        if ($this->debugMode) $params['mlc__debug'] = 1;
        $params = $params + MLC::stats();
        $postVars = MLC::query_stringify($params);
        $host = parse_url(self::BASE_URL, PHP_URL_HOST);
        $auth = base64_encode($this->accountId . ':' . $this->apiToken);

        $payload = "POST " . self::BASE_URL . 'api/' . self::API_VERSION . '/leads' . " HTTP/1.0\r\n";
        $payload .= "Host: " . $host . "\r\n";
        $payload .= "User-Agent: " . self::UA ."\r\n";
        $payload .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $payload .= "Authorization: Basic " . $auth . "\r\n";
        $payload .= "Content-length: " . strlen($postVars) . "\r\n";
        $payload .= "Connection: close \r\n";
        $payload .= "X-API-Version: " . self::API_REVISION . "\r\n";
        $payload .= "Accept: application/json\r\n\r\n";
        $payload .= $postVars;

        ob_start();
        if (self::SSL) $sock = fsockopen("ssl://".$host, 443, $errno, $errstr, 30);
        else $sock = fsockopen($host, 8080, $errno, $errstr, 30);

        if (!$sock) {
            ob_end_clean();
            return false;
        }
        $response = '';
        fwrite($sock, $payload);
        stream_set_timeout($sock, 60); // 60 second timeout
        $info = stream_get_meta_data($sock);
        while ((!feof($sock)) && (!$info["timed_out"])) {
            $response .= fread($sock, 8192); // 8k chunks
            $info = stream_get_meta_data($sock);
        }
        fclose($sock);
        ob_end_clean();

        if ($info["timed_out"]) return false;

        return $response;
    }

}

?>